#include <stdio.h>

int addition(int *a,int *b)
{
   int sum;
   sum=*a+*b;
   return sum;
}
int difference (int *a,int *b)
{
   int dif;
   dif=*a-*b;
   return dif;
}
int multiply(int *a,int *b)
{
   int prod;
   prod=(*a)*(*b);
   return prod;
}
float division(int *a,int *b)
{
   float divide;
   divide=(float)(*a)/(*b);
   return divide;
}
int remainder(int *a,int *b)
{
   int rem;
   rem=(*a)%(*b);
   return rem;
}

int main()
{
   int a,b,sum,dif,prod,rem;
   float divide;
   printf("Enter two integers\n");
   scanf("%d%d",&a,&b);
   sum=addition(&a,&b);
   dif=difference (&a,&b);
   prod=multiply (&a,&b);
   divide=division (&a,&b);
   rem=remainder (&a,&b);
   printf("Sum=%d\n",sum);
   printf("Difference=%d\n",dif);
   printf("Product=%d\n",prod);
   printf("Division=%f\n", divide);
   printf("Remander=%d\n",rem);
   return 0;
}
