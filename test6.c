#include<stdio.h>
int input()
{
    int n;
    printf("Enter the number of integers\n");
    scanf("%d",&n);
    return n;
}
float compute(int n)
{
    int sum=0,i;
    float avg;
    printf("Enter the elements\n");
    int a[n];
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
        sum=sum+a[i];
    }
    avg=(float)sum/n;
    return avg;
}
void output(int n,float avg)
{
    printf("The average of %d is  %f\n",n,avg);
}
int main()
{
    int n;
    float avg;
    n=input();
    avg=compute(n);
    output(n,avg);
    return 0;
}

