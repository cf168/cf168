#include<stdio.h>

void input(int *a,int *b)
{
   printf("Enter two numbers\n");
   scanf("%d%d",a,b);
   printf ("The numbers before swapping\na=%d\nb=%d\n",*a,*b);
}
void swap(int *a,int *b)
{
   
   int temp;
   temp=*a;
   *a=*b;
   *b=temp;
}
int main()
{
   int a,b;
   input(&a,&b);
   swap(&a,&b);
   printf ("The numbers after swapping\na=%d\nb=%d\n",a,b);
   return 0;
}