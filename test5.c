#include<stdio.h>
int input()
{
	int n;
	printf("Enter the number whose multiples has to be printed from 1 to 100\n");
	scanf("%d",&n);
	return n;
}
int compute(int n)
{
	int i;
	for(i=1;i<=100;i++)
	{
		if(i%n==0)
		{
			printf("%d\n",i);
		}
	}
	return 0;
}
int main()
{
	int n,i;
	n=input();
	compute(n);
	return 0;
}

