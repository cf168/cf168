#include <stdio.h>

int main()
{
    int i,j;
    int a[3][3],b[3][3];
    printf("Enter the elements of matrix\n");
    
    for(i=0;i<3;i++)
    {
        for(j=0;j<3;j++)
        {
            scanf("%d",&a[i][j]);
        }
        
    }
    printf("The elements of the matrix are\n");
    for(i=0;i<3;i++)
    {
    printf("\n");
    for(j=0;j<3;j++)
    {
    printf("\t%d",a[i][j]);
    }
    }
    for(i=0;i<3;i++)
    {
    for(j=0;j<3;j++)
    {
    b[j][i]=a[i][j];
    }
    }
    printf("\nThe TRANSPOSE of the given matrix is\n");
    for(i=0;i<3;i++)
    {
        for(j=0;j<3;j++)
        {
            printf("%d\t",b[i][j]);
        }
        printf("\n");
    }
    
    return 0;
}