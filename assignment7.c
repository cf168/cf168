#include <stdio.h>

int main()
{
    int i,j;
    int a[4][3];

    for(i=0;i<4;i++)
    {
        printf("Enter the sales of the  three products sold by salesman %d\n",(i+1));
        for(j=0;j<3;j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
    printf("\n");
    for(i=0;i<4;i++)
    {
        for(j=0;j<3;j++)
        {
            printf("%d\t",a[i][j]);
        }
        printf("\n");
    }
    int t_sales,p_sales;
    for(i=0;i<4;i++)
    {
        t_sales=0;
        for(j=0;j<3;j++)
        {

            t_sales=t_sales+a[i][j];

        }
        printf("The total number of sales of the salesman %d=%d\n",(i+1),t_sales);
    }
    printf("\n");
    for(j=0;j<3;j++)
    {
        p_sales=0;
        for(i=0;i<4;i++)
        {

            p_sales=p_sales+a[i][j];

        }
        printf("The total number of sales of the product %d=%d\n",(j+1),p_sales);
    }
    return 0;
}   

